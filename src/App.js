import React, { useRef } from 'react';

function App() {
  const input = useRef(null)

  const handleButtonClick = function () {
    console.log(input.current.value)
  }
  
  return <div className="App">
      <input type={'text'} ref={input}/>
      <button onClick={handleButtonClick}>Récupérer la valeur</button>
    </div>
}

export default App;
