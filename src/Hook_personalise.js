import React, { useState,useEffect } from 'react';

function useIncrement(initial,step=1) {
  const [count,setCount] = useState(initial)
  
  const increment = () => {
    setCount(count => count + step)
  }

  return [count,increment]
}

function useAutoIncrement(initialValue=10,step=1) {
  const [value,setValue] = useIncrement(initialValue,step)
  useEffect(() => {
    const timer = window.setInterval(() => {
      setValue()
    },1000)
    return () => {
      window.clearInterval(timer)
    };
  }, []);

  return [value,setValue]
}

function useToggle(initialValue = true) {
  const [value,setValue] = useState(initialValue)
  const toggle = function (params) {
    setValue(value => !value)
  }
  return [value,toggle]
}

function Compteur(){
  const [count,increment] = useAutoIncrement()

  return <button onClick={increment}>Incrémenter {count}</button>
}

function useFetch(url){
  const [state,setState] = useState({
    items: [],
    loading: true
  })

  useEffect(() => {
    (async function(){
      const response = await fetch(url)
      const responseData = await response.json()
      
      if(response.ok){
        setState({
          items: responseData,
          loading: false
        })
      } else {
        alert(JSON.stringify(responseData))
        // On change juste la valeur de loading et pour que la fonction s'execute tout de suite il faut la mettre entre parenthèse.
        setState(state => ({...state,loading:false}) )
      }
      
    })();
  }, []);

  return [
    state.items,
    state.loading
  ]
}

function PostTable() {
  const [posts,load] = useFetch('https://jsonplaceholder.typicode.com/posts?_limit=10')
  
  if(load){
    return 'Chargement ...'
  }

  return <table className={'table table-bordered'}>
    <thead>
      <tr>
        <th className={'text-center'}>ID</th>
        <th className={'text-center'}>TITLE</th>
        <th className={'text-center'}>CONTENT</th>
      </tr>
    </thead>
    <tbody>
      {posts.map(p => 
        <tr key={p.id}>
          <td>{p.id}</td>
          <td>{p.title}</td>
          <td>{p.body}</td>
        </tr>
      )}
      
    </tbody>
  </table>
}

function TodoList() {
  const [todos,loading] = useFetch('https://jsonplaceholder.typicode.com/todos?_limit=10')
  
  if (loading) {
    return 'Chargement ...'
  }

  return <ul>
    {todos.map(t => <li key={t.id}>{t.title}</li>)}
  </ul>
}

function App() {
  const [compteurVisible, toggleCompteur] = useToggle(true)
  return (
    <div className="App">
      Afficher le compteur : <input type={'checkbox'} onChange={toggleCompteur} checked={compteurVisible}/>
      <br/>
      {compteurVisible && <Compteur/>}
      <br/>
      <TodoList/>
      <br/>
      <PostTable/>
    </div>
  );
}

export default App;
