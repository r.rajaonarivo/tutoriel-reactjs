import React,{useState,useEffect} from 'react';

function useIncrement(initial, step) {
  const [count, setCount] = useState(initial)
  const increment = () => {
    setCount(count => count + step)
  }

  return [count,increment]
}

function Compteur(){
  const [count, increment] = useIncrement(0,1)

  // useEffect est indépendant
  useEffect(() => {
    const timer = window.setInterval(() =>{
      increment()
    },1000)

    return function () {
      window.clearInterval(timer)
    }
  },[])

  // useEffect est l'équivalence de componentDidMount, use Effect est effectué quand la valeur de count est modifié
  useEffect(() =>{
    document.title = "Compteur" + count
    
  },[count])
  
 
  return <div>
    <button onClick={increment}>COunt : {count}</button>
  </div>
}

function App() {
  return (
    <div className="App">
      <Compteur/>
    </div>
  );
}

export default App;
